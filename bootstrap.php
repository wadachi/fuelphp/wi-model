<?php
/**
 * Wadachi FuelPHP ORM Model Package
 *
 * An ORM model with field trimming, data validation, and mass assignment capabilities.
 *
 * @package    wi-model
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Package::load([
  'orm',
  'wi-json',
]);

\Autoloader::add_classes([
  'Wi\\Observer_Trimming' => __DIR__.'/classes/observer/trimming.php',
  'Wi\\Observer_Typing' => __DIR__.'/classes/observer/typing.php',
  'Wi\\Model' => __DIR__.'/classes/model.php',
  'Wi\\Query' => __DIR__.'/classes/query.php',
]);
