<?php
/**
 * Wadachi FuelPHP ORM Model Package
 *
 * An ORM model with field trimming, data validation, and mass assignment capabilities.
 *
 * @package    wi-model
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

/**
 * 日付設定
 */
return [
	/**
	 * 変換パターン
	 */
	'patterns' => [
		'mysql_date'=> '%Y-%m-%d',
	]
];
