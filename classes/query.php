<?php
/**
 * Wadachi FuelPHP ORM Model Package
 *
 * An ORM model with field trimming, data validation, and mass assignment capabilities.
 *
 * @package    wi-model
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * クエリー　ビルダ
 */
class Query extends \Orm\Query
{
  /**
   * 指定されたフィールド内に用語を検索する
   * "いちご +みかん -すいか くるみ" で検索できる
   * "(いちご AND みかん AND NOT すいか) OR (くるみ)" になる
   *
   * @access public
   * @param string フィールド名
   * @param string 用語のスペース区切り文字列）
   * @return Query クエリー　ビルダ
   */
  public function where_contains()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $condition = func_get_args();
    is_array(reset($condition)) and $condition = reset($condition);

    if (count($condition) < 2 || empty(end($condition)))
    {
      return this;
    }

    $field = $condition[0];
    $search = end($condition);                                 // "   50%  green  ++++++blue   ---_RED_   "
    $search = trim($search);                                   // "50%  green  ++++++blue   ---_RED_"
    $search = str_replace('%', '\%', $search);                 // "50\%  green  ++++++blue   ---_RED_"
    $search = str_replace('_', '\_', $search);                 // "50\%  green  ++++++blue   ---\_RED\_"
    $search = preg_replace('/[\s]+/', ' ', $search);           // "50\% green ++++++blue ---\_RED\_"
    $search = preg_replace('/[\s]?[\+＋]+/', '+', $search);    // "50\% green+blue ---\_RED\_"
    $search = preg_replace('/[\s]?[\-ー]+/', '-', $search);    // "50\% green+blue-\_RED\_"

    $this->and_where_open();
    $or_groups = preg_split('/\s/', $search);                  // ["50\%", "green+blue-\_RED\_"]
    $operator = '+';

    foreach ($or_groups as $or_group)
    {
      $this->or_where_open();                                  // ["green", "+", "blue", "-", "\_RED\_"]
      $and_groups = preg_split('/([\+\-])/', $or_group, -1, PREG_SPLIT_DELIM_CAPTURE);

      foreach ($and_groups as $token)
      {
        if ($token === '+' || $token === '-')
        {
          $operator = $token;
          continue;
        }

        switch ($operator)
        {
          case '+':
            $this->where($field, 'LIKE', '%'.$token.'%');
            break;
          case '-':
            $this->where($field, 'NOT LIKE', '%'.$token.'%');
            break;
        }
      }  // foreach $and_groups

      $this->or_where_close();
    }  // foreach $or_groups

    $this->and_where_close();

    return $this;
  }// </editor-fold>

  /**
   * 指定されたフィールド内にすべての用語を検索する
   *
   * @access public
   * @param string フィールド名
   * @param string 用語のスペース区切り文字列）
   * @return Query クエリー　ビルダ
   */
  public function where_contains_all()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $condition = func_get_args();
    is_array(reset($condition)) and $condition = reset($condition);
    $last = count($condition) - 1;

    if ($last > 0 && !empty($condition[$last]))
    {
      $condition[$last] = preg_replace('/[\s\-]+/', '+', $condition[$last]);
      return $this->where_contains($condition);
    }

    return this;
  }// </editor-fold>

  /**
   * 指定されたフィールド内に存在していないの用語を検索する
   *
   * @access public
   * @param string フィールド名
   * @param string 用語のスペース区切り文字列）
   * @return Query クエリー　ビルダ
   */
  public function where_contains_none()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $condition = func_get_args();
    is_array(reset($condition)) and $condition = reset($condition);
    $last = count($condition) - 1;

    if ($last > 0 && !empty($condition[$last]))
    {
      $condition[$last] = '-'.preg_replace('/[\s\+]+/', '-', $condition[$last]);
      return $this->where_contains($condition);
    }

    return this;
  }// </editor-fold>

  /**
   * 「WHERE ... AND ...」条件を設定する
   *
   * @param string フィールド名
   * @param string 比較タイプ（省略可能）
   * @param string 比較値
   * @return Query クエリー　ビルダ
   */
  public function where()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $condition = func_get_args();
    is_array(reset($condition)) and $condition = reset($condition);

    switch (\Arr::get($condition, '1'))
    {
      case 'contains':
        return $this->where_contains($condition);
      case 'contains_all':
        return $this->where_contains_all($condition);
      case 'contains_none':
        return $this->where_contains_none($condition);
    }

    return parent::where($condition);
  }// </editor-fold>

  /**
   * 「WHERE ... OR ...」条件を設定する
   *
   * @param string フィールド名
   * @param string 比較タイプ（省略可能）
   * @param string 比較値
   * @return Query クエリー　ビルダ
   */
  public function or_where()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $condition = func_get_args();
    is_array(reset($condition)) and $condition = reset($condition);

    switch (\Arr::get($condition, '1'))
    {
      case 'contains':
        $this->or_where_open();
        $this->where_contains($condition);
        $this->or_where_close();
        return $this;
      case 'contains_all':
        $this->or_where_open();
        $this->where_contains_all($condition);
        $this->or_where_close();
        return $this;
      case 'contains_none':
        $this->or_where_open();
        $this->where_contains_none($condition);
        $this->or_where_close();
        return $this;
    }

    return parent::or_where($condition);
  }// </editor-fold>
}
