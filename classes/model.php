<?php
/**
 * Wadachi FuelPHP ORM Model Package
 *
 * An ORM model with field trimming, data validation, and mass assignment capabilities.
 *
 * @package    wi-model
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * ベース モデル
 */
abstract class Model extends \Orm\Model
{
  // オブザーバー 設定
  protected static $_observers = [
    '\\Orm\\Observer_Validation' => [
      'events' => ['before_save']
    ],

    '\\Wi\\Observer_Trimming' => [
      'events' => ['before_save']
    ],

    '\\Wi\\Observer_Typing' => [
      'events' => ['before_save', 'after_save', 'after_load']
    ]
  ];

  /**
   * このモデルのキャッシュを削除する
   *
   * @access public
   */
  public static function flush_cache()// <editor-fold defaultstate="collapsed" desc="...">
  {
    $class = get_called_class();
    unset(static::$_cached_objects[$class]);
  }// </editor-fold>

  /**
   * 新しいクエリを作成する
   *
   * @access public
   * @param array オプション
   * @return Query クエリー　ビルダ
   */
  public static function query($options = array())// <editor-fold defaultstate="collapsed" desc="...">
  {
    return Query::forge(get_called_class(), array(static::connection(), static::connection(true)), $options);
  }// </editor-fold>

  /**
   * 質量割り当ての前に許容特性をフィルタする
   *
   * @access public
   * @param string|array $property プロパティ
   * @param string $value プロパティは文字列の場合
   * @throws \FuelException Primary key on model cannot be changed
   * @throws \InvalidArgumentException You need to pass both a property name and a value to set()
   * @throws \FrozenObject No changes allowed
   * @return \Wi\Model モデル
   */
  public function set($property, $value = null)// <editor-fold defaultstate="collapsed" desc="...">
  {
    // 質量割り当てフィルタを適用する
    if (is_array($property))
    {
      foreach (array_keys($property) as $key)
      {
        $config = static::property($key);

        if (!isset($config['allow_mass_assignment']) || !$config['allow_mass_assignment'])
        {
          unset($property[$key]);
        }
      }
    }

    return parent::set($property, $value);
  }// </editor-fold>

  /**
   * 配列にこのオブジェクトを変換することができる
   * \Orm\Modelからコピー取って変更した
   *
   * @param boolean $custom カスタムデータを使うかどうか
   * @param boolean $recurse 再帰するかどうか
   * @param boolean $eav フィールドの除外するかどうか
   * @internal param \Orm\whether $bool カスタムデータ配列を含めるかどうか
   * @return array 配列でこのオブジェクト
   */
  public function to_array($custom = false, $recurse = false, $eav = false)// <editor-fold defaultstate="collapsed" desc="...">
  {
    // storage for the result
    $array = array();

    // reset the references array on first call
    $recurse or static::$to_array_references = array(get_class($this));

    // make sure all data is scalar or array
    if ($custom)
    {
      foreach ($this->_custom_data as $key => $val)
      {
        if (is_object($val) && !($val instanceof \Fuel\Core\Date)) // <--- 変更された（日付）
        {
          if (method_exists($val, '__toString'))
          {
            $val = (string) $val;
          }
          else
          {
            $val = get_object_vars($val);
          }
        }
        $array[$key] = $val;
      }
    }

    // make sure all data is scalar or array
    foreach ($this->_data as $key => $val)
    {
      if (is_object($val) && !($val instanceof \Fuel\Core\Date)) // <--- 変更された（日付）
      {
        if (method_exists($val, '__toString'))
        {
          $val = (string) $val;
        }
        else
        {
          $val = get_object_vars($val);
        }
      }
      $array[$key] = $val;
    }

    // convert relations
    foreach ($this->_data_relations as $name => $rel)
    {
      if (is_array($rel))
      {
        $array[$name] = array();
        if ( ! empty($rel))
        {
          if ( ! in_array(get_class(reset($rel)), static::$to_array_references))
          {
            static::$to_array_references[] = get_class(reset($rel));
            foreach ($rel as $id => $r)
            {
              $array[$name][$id] = $r->to_array($custom, true, $eav);
              array_pop(static::$to_array_references);
            }
          }
        }
      }
      else
      {
        if ( ! in_array(get_class($rel), static::$to_array_references))
        {
          if (is_null($rel))
          {
            $array[$name] = null;
          }
          else
          {
            static::$to_array_references[] = get_class($rel);
            $array[$name] = $rel->to_array($custom, true, $eav);
            array_pop(static::$to_array_references);
          }
        }
      }
    }

    // get eav relations
    if ($eav and property_exists(get_called_class(), '_eav'))
    {
      // loop through the defined EAV containers
      foreach (static::$_eav as $rel => $settings)
      {
        // normalize the container definition, could be string or array
        if (is_string($settings))
        {
          $rel = $settings;
          $settings = array();
        }

        // determine attribute and value column names
        $attr = \Arr::get($settings, 'attribute', 'attribute');
        $val  = \Arr::get($settings, 'value', 'value');

        // check if relation is present
        if (array_key_exists($rel, $array))
        {
          // get eav properties
          $container = \Arr::assoc_to_keyval($array[$rel], $attr, $val);

          // merge eav properties to array without overwritting anything
          $array = array_merge($container, $array);

          // we don't need this relation anymore
          unset($array[$rel]);
        }
      }
    }

    // strip any excluded values from the array
    foreach (static::get_to_array_exclude() as $key)
    {
      if (array_key_exists($key, $array))
      {
        unset($array[$key]);
      }
    }

    return $array;
  }// </editor-fold>
}
