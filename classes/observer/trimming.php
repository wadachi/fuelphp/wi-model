<?php
/**
 * Wadachi FuelPHP ORM Model Package
 *
 * An ORM model with field trimming, data validation, and mass assignment capabilities.
 *
 * @package    wi-model
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * 文字列のトリム オブザーバー
 */
class Observer_Trimming extends \Orm\Observer
{
  /**
   * 保存する前に、文字列をトリムする
   * 空の文字列がnullになる
   *
   * @access public
   * @param \Orm\Model $model モデル
   */
  public function before_save(\Orm\Model $model)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $properties = $model->properties();

    foreach (array_keys($properties) as $property)
    {
      $value = $model[$property];

      if (is_string($value))
      {
        $value = trim($value);
        $value = $value === '' ? null : $value;
        $model[$property] = $value;
      }
    }
  }// </editor-fold>
}
