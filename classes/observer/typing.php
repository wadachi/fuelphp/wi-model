<?php
/**
 * Wadachi FuelPHP ORM Model Package
 *
 * An ORM model with field trimming, data validation, and mass assignment capabilities.
 *
 * @package    wi-model
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * 値型キャスト オブザーバー
 */
class Observer_Typing extends \Orm\Observer_Typing
{
  /**
   * 静的コンストラクタ
   * 型キャスト マップに追加する
   *
   * @access public
   */
  public static function _init()// <editor-fold defaultstate="collapsed" desc="...">
  {
    // MySql DATE 型キャスト
    static::$type_mappings['date_unix'] = 'date';
    static::$type_mappings['date_mysql'] = 'date';
    static::$type_methods['date'] = [
      'before' => '\\Wi\\Observer_Typing::type_date_encode',
      'after'  => '\\Wi\\Observer_Typing::type_date_decode'
    ];

    // JSON DateTime 型キャスト
    static::$type_methods['datetime_json'] = [
      'before' => '\\Wi\\Observer_Typing::type_datetime_json_encode',
      'after'  => '\\Wi\\Observer_Typing::type_datetime_json_decode'
    ];
  }// </editor-fold>

  /**
   * DBのタイムスタンプを取得し、Dateオブジェクトに変換する
   *
   * @access public
   * @param string $var 値
   * @param array $settings いずれかのオプションが渡される
   * @return \Fuel\Core\Date 日付
   */
  public static function type_date_decode($var, array $settings)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if ($settings['data_type'] == 'date_mysql')
    {
      // deal with a 'nulled' date, which according to MySQL is valid enough to store?
      if (+filter_var($var, FILTER_SANITIZE_NUMBER_INT) === 0)  // '0000-00-00' -> '00000000' -> 0
      {
        if (array_key_exists('null', $settings) and $settings['null'] === false)
        {
          throw new InvalidContentType('Value '.$var.' is not a valid date and can not be converted to a Date object.');
        }

        return null;
      }

      return \Date::create_from_string($var, 'mysql_date');
    }

    return \Date::forge($var);
  }// </editor-fold>

  /**
   * Dateインスタンスか文字列を取得し、DBのタイムスタンプに変換する
   *
   * @access public
   * @param mixed $var 値
   * @param array $settings いずれかのオプションが渡される
   * @throws InvalidContentType
   * @return int|string タイムスタンプ
   */
  public static function type_date_encode($var, array $settings)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (is_string($var))
    {
      try
      {
        $var = \Fuel\Core\Date::create_from_string($var, 'mysql_date');
      }
      catch (Exception $e)
      {
        $e->getMessage();
        return null;
      }
    }

    if ( ! $var instanceof \Fuel\Core\Date)
    {
      throw new InvalidContentType('Value must be an instance of the Date class.');
    }

    if ($settings['data_type'] == 'date_mysql')
    {
      return $var->format('mysql_date');
    }

    return $var->get_timestamp();
  }// </editor-fold>

  /**
   * DBの日時を取得し、Json_DateTimeオブジェクトに変換する
   *
   * @access public
   * @param string $var 値
   * @param array $settings いずれかのオプションが渡される
   * @return \Wi\Json_DateTime 日時
   */
  public static function type_datetime_json_decode($var, array $settings)// <editor-fold defaultstate="collapsed" desc="...">
  {
    return new Json_DateTime($var);
  }// </editor-fold>

  /**
   * Json_DateTimeか文字列を取得し、DBの日時に変換する
   *
   * @access public
   * @param mixed $var 値
   * @param array $settings いずれかのオプションが渡される
   * @throws InvalidContentType
   * @return string 日時
   */
  public static function type_datetime_json_encode($var, array $settings)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if ($var instanceof Json_DateTime)
    {
      return $var->format('Y-m-d H:i:s.u');
    }

    if (is_string($var))
    {
      return $var;
    }

    throw new InvalidContentType('Value must be an instance of the DateTime class or a string.');
  }// </editor-fold>
}
